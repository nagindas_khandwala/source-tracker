﻿<%@ Page Title="" Language="C#" MasterPageFile="~/client.master" AutoEventWireup="true"
    CodeFile="prd_display.aspx.cs" Inherits="prd_display" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:DataList ID="DataList1" runat="server" DataKeyField="ProductID" DataSourceID="SqlDataSource1"
        RepeatColumns="3">
        <ItemTemplate>
            <div style="margin: 1.5%;text-align:center">
                <a id="A1" href='<%#"single.aspx?ProductID="+Eval("ProductID") %>' runat="server" text="" 
                    causesvalidation="false">
                    <asp:Label ID="ProductIDLabel" runat="server" Text='<%# Eval("ProductID") %>' Style="display: none" />
                    <br />
                    <asp:Image ID="Image1" runat="server" ImageUrl='<%# "~/admin/imges/"+Eval("img")  %>'
                        Height="200px" Width="200px" />
                    <br />
                    <asp:Label ID="Product_NameLabel" runat="server" Text='<%# Eval("Product_Name") %>' />
                    <br />
                    </a>
                    </div>
        </ItemTemplate>
    </asp:DataList>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:istsdataConnectionString %>"
        SelectCommand="SELECT [ProductID], [Product_Name], [img] FROM [ProductDetails]">
    </asp:SqlDataSource>
</asp:Content>
