﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/adminpanel.master" AutoEventWireup="true" CodeFile="admindetails.aspx.cs" Inherits="admin_admindetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="style.css" type="text/css" rel="Stylesheet"/>
    <style type="text/css">
        .style2
        {
            width: 328px;
        }
         
        .style3
        {
            width: 131px;
            height: 122px;
        }
        .style21
        {
            width: 131px;
            height: 37px;
        }
        .style22
        {
            width: 328px;
            height: 37px;
        }
        .style23
        {
            width: 131px;
            height: 38px;
        }
        .style24
        {
            width: 328px;
            height: 38px;
        }
        .style25
        {
            width: 131px;
            height: 36px;
        }
        .style26
        {
            width: 328px;
            height: 36px;
        }
        .style27
        {
            width: 131px;
            height: 35px;
        }
        .style28
        {
            width: 328px;
            height: 35px;
        }
         
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1 class="contenth1" align="center"><b>Admin Details</b></h1>
    <table border="5" cellpadding="5" cellspacing="5" 
        style="width:100%; font-size: small;" rules="none">
        <tr>
            <td class="style21"><b>
                UserName</b></td>
            <td class="style22">
                <asp:TextBox ID="admin_name" runat="server" Width="300px" CssClass="textbox" 
                    Height="22px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style23"><b>
                EmailID</b></td>
            <td class="style24">
                <asp:TextBox ID="admin_email" runat="server" Width="300px" Height="22px"  PlaceHolder="ex:example@gmail.com" CssClass="textbox"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style21"><b>
                Password</b></td>
            <td class="style22">
                <asp:TextBox ID="admin_paswd" runat="server" Width="300px" TextMode="Password" Height="22px" CssClass="textbox"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style25"><b>
                Re-enter Password</b></td>
            <td class="style26">
                <asp:TextBox ID="conf_pswd" runat="server" Width="300px" Height="22px" TextMode="Password" CssClass="textbox"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style27"><b>
                Questions(Optional)</b></td>
            <td class="style28">
                <asp:DropDownList ID="ddl" runat="server" Height="25px" Width="300px"   CssClass="textbox">
                    <asp:ListItem>select Questions</asp:ListItem>
                    <asp:ListItem>Place of Birth ?</asp:ListItem>
                    <asp:ListItem>What is your faviourate song?</asp:ListItem>
                    <asp:ListItem>what is your mother&#39;s name?</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style23"><b>
                Answer</b></td>
            <td class="style24">
                <asp:TextBox ID="admin_ans" runat="server" Width="300px" Height="22px"  CssClass="textbox"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style3">
                &nbsp;</td>
            <td class="style2">
            <asp:Button ID="btn_adm_submit" runat="server" Height="42px" Text="Add" ToolTip="Add" 
                Width="101px" CssClass="btn" onclick="btn_adm_submit_Click" />
                &nbsp;&nbsp;
            <asp:Button ID="btn_adm_reset" runat="server" Height="43px" Text="Reset" ToolTip="Reset" 
                Width="101px" PostBackUrl="~/admin/admindetails.aspx" CssClass="btn" 
                    onclick="btn_adm_reset_Click"/>
            
            </td>
        </tr>
        </table>
    <br />
    <div>

        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
            DataKeyNames="AdminID" DataSourceID="SqlDataSource1" CellPadding="4" 
            ForeColor="#333333" GridLines="None">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:TemplateField HeaderText="AdminID" InsertVisible="False" 
                    SortExpression="AdminID">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox" runat="server" Text='<%# Bind("AdminID") %>' ReadOnly="true"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("AdminID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="UserName" SortExpression="UserName">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("UserName") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("UserName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="EmailID" SortExpression="EmailID">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("EmailID") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("EmailID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Password" SortExpression="Password">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Password") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label4" runat="server" Text='<%# Bind("Password") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Questions" SortExpression="Questions">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Questions") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label5" runat="server" Text='<%# Bind("Questions") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Answers" SortExpression="Answers">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("Answers") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label6" runat="server" Text='<%# Bind("Answers") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False">
                    <EditItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" 
                            CommandName="Update" Text="Update"></asp:LinkButton>
                        &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" 
                            CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" 
                            CommandName="Edit" Text="Edit"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton3" runat="server" CausesValidation="False" 
                            CommandName="Delete" Text="Delete"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EditRowStyle BackColor="#2461BF" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F5F7FB" />
            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
            <SortedDescendingCellStyle BackColor="#E9EBEF" />
            <SortedDescendingHeaderStyle BackColor="#4870BE" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:istsdataConnectionString %>" 
            SelectCommand="SELECT AdminDetails.* FROM AdminDetails" 
            DeleteCommand="DELETE FROM AdminDetails where AdminID=@AdminID" 
            
            UpdateCommand="UPDATE AdminDetails SET UserName = @UserName, EmailID = @EmailID, Questions = @Questions, Answers = @Answers WHERE (AdminID = @AdminID)">
            <DeleteParameters>
                <asp:Parameter Name="AdminID" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="UserName" />
                <asp:Parameter Name="EmailID" />
                <asp:Parameter Name="Questions" />
                <asp:Parameter Name="Answers" />
                <asp:Parameter Name="AdminID" />
            </UpdateParameters>
        </asp:SqlDataSource>

    </div>
</asp:Content>


