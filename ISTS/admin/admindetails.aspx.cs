﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Configuration;
using System.Data.SqlClient;

public partial class admin_admindetails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        GridView1.Visible = true;
    }
    protected void btn_adm_submit_Click(object sender, EventArgs e)
    {
        SqlConnection con = null;
        SqlCommand cmd = null;

        try
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["istsdataConnectionString"].ConnectionString);

            String sql = "insert into AdminDetails(UserName,EmailID,Password,Questions,Answers) values(@UserName,@EmailID,@Password,@Questions,@Answers)";
            con.Open();
            cmd = new SqlCommand(sql, con);
           
            cmd.Parameters.AddWithValue("@UserName",admin_name.Text);
            cmd.Parameters.AddWithValue("@EmailID", admin_email.Text);
            cmd.Parameters.AddWithValue("@Password",conf_pswd.Text);
            cmd.Parameters.AddWithValue("@Questions",ddl.SelectedValue);
            cmd.Parameters.AddWithValue("@Answers",admin_ans.Text);
           

            cmd.Connection = con;
            
            admin_name.Text = "";
            admin_email.Text = "";
            conf_pswd.Text = "";
            admin_ans.Text = "";
            ddl.SelectedIndex = 0;

            cmd.ExecuteNonQuery();
            /*string message = "Record Inserted Successfully.";

            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            sb.Append("<script type = 'text/javascript'>");

            sb.Append("window.onload=function(){");

            sb.Append("alert('");

            sb.Append(message);

            sb.Append("')};");

            sb.Append("</script>");

            ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb.ToString());*/
            con.Close();
            Response.Redirect("admindetails.aspx");
        }
        catch (Exception)
        {
            // Console.WriteLine(e1.Message);
            throw;
        }
    }

   
    protected void btn_adm_reset_Click(object sender, EventArgs e)
    {
        Response.Redirect("admindetails.aspx");
    }
}