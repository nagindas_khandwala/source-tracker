﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/adminpanel.master" AutoEventWireup="true"
    CodeFile="product.aspx.cs" Inherits="admin_product" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="style.css" rel="Stylesheet" type="text/css" />
    <link href="Button.css" rel="Stylesheet" type="text/css" />
    <style type="text/css">
        .text style
        {
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
        }
        #prd_spec
        {
            width: 300px;
        }
        .style6
        {
            width: 393px;
        }
        .style7
        {
            width: 184px;
        }
        .style8
        {
            width: 223px;
        }
        .style9
        {
            width: 454px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div>
        <h1 class="contenth1" align="center">
            <b>Product Details</b></h1>
    </div>
    <table cellpadding="5" cellspacing="5" rules="none" style="width: 100%; font-size: small;"
        border="5" runat="server">
        <tr>
            <td class="style7">
                <b>Product_Name </b>
            </td>
            <td class="style6" align="center">
                <asp:TextBox ID="prd_name" Width="300px" Height="22px" CssClass="textbox" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style7">
                <b>Quantity </b>
            </td>
            <td class="style6" align="center">
                <asp:TextBox ID="prd_qty" runat="server" Width="300px" Height="22px" CssClass="textbox"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style7">
                <b>Price</b>
            </td>
            <td class="style6" align="center">
                <asp:TextBox ID="prd_price" runat="server" Width="300px" Height="22px" CssClass="textbox"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style7">
                <b>Description :</b>
            </td>
        </tr>
        <tr>
            <td class="style7">
                <b>Product_specification</b>
            </td>
            <td class="style6" align="center">
                <asp:TextBox ID="prd_desc" runat="server" Columns="40" Rows="5" Style="background: white;
                    border: 1px solid #78BBE6; border-radius: 5px; box-shadow: 0 0 5px 3px #78BBE6;
                    color: #666; outline: none;" TextMode="MultiLine" Height="63px" Width="296px"></asp:TextBox><br />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="style7">
                <b>Depletion_Time</b>
            </td>
            <td class="style6" align="center">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:TextBox ID="prd_expire" runat="server" Width="294px" CssClass="textbox" Height="22px"></asp:TextBox>
                &nbsp; days.
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="style7">
                <b>Image Upload</b>
            </td>
            <td class="style6" align="center">
                <asp:FileUpload ID="FileUpload1" runat="server" CssClass="textbox" Width="250px" />
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="style7">
                &nbsp;
            </td>
            <td class="style6" align="center">
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="style7">
                &nbsp;
            </td>
            <td class="style6" align="center">
                <asp:Button ID="Button1" runat="server" Text="Add" CssClass="btn" Height="42px" Width="99px"
                    OnClick="Button1_Click1" />
                &nbsp;&nbsp;&nbsp;
                <asp:Button ID="Button2" runat="server" Text="Reset" CssClass="btn" Height="41px"
                    Width="96px" OnClick="Button2_Click" />
                &nbsp;&nbsp;&nbsp;
             <%--   <asp:Button ID="Button3" runat="server" Text="update qty" CssClass="btn" Height="42px"
                    Width="127px" OnClick="Button3_Click" />--%>
            </td>
        </tr>
    </table>
    <div>
    </div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div>
                <table style="width: 100%;">
                    <tr>
                        <td class="style8">
                            &nbsp;
                        </td>
                        <td class="style9">
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="style8">
                            &nbsp;
                        </td>
                        <td class="style9">
                           <%-- <asp:RadioButton ID="RadioButton1" runat="server" GroupName="Stk" Text="Stock" 
                                OnCheckedChanged="RadioButton1_CheckedChanged" 
                                onload="RadioButton1_CheckedChanged" />--%>

                            <asp:Button ID="Button4" runat="server" Text="In stock" 
                                onclick="Button4_Click" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          <%--  <asp:RadioButton ID="RadioButton2" runat="server" GroupName="Stk" Text="Out of stock"
                                OnCheckedChanged="RadioButton2_CheckedChanged" />--%>
                                <asp:Button ID="Button5" runat="server" Text="out of stock" 
                                onclick="Button5_Click" />

                               <%-- <label class="switch">
                                <input type="checkbox">
                                <div class="slider round"></div>
                                </label>--%>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="style8">
                            &nbsp;
                        </td>
                        <td class="style9">
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <div>
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4"
                        DataKeyNames="ProductID" DataSourceID="SqlDataSource1" ForeColor="#333333" GridLines="None">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:TemplateField HeaderText="ProductID" InsertVisible="False" SortExpression="ProductID">
                                <EditItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("ProductID") %>' ReadOnly="true"></asp:Label>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("ProductID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Product_Name" SortExpression="Product_Name">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Product_Name") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Product_Name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Quantity" SortExpression="Quantity">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Quantity") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("Quantity") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Price" SortExpression="Price">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Price") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("Price") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Product_Specification" SortExpression="Product_Specification">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Product_Specification") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label5" runat="server" Text='<%# Bind("Product_Specification") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Depletion_Time" SortExpression="Depletion_Time">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("Depletion_Time") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label6" runat="server" Text='<%# Bind("Depletion_Time") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="img" SortExpression="img">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("img") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label7" runat="server" Text='<%# Bind("img") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Image ID="Image1" runat="server" ImageUrl='<%# "~/admin/imges/"+Eval("img")  %>'
                                        Height="50px" Width="50px" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <EditItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update"
                                        Text="Update"></asp:LinkButton>
                                    &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel"
                                        Text="Cancel"></asp:LinkButton>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit"
                                        Text="Edit"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:istsdataConnectionString %>"
                        SelectCommand="SELECT ProductDetails.* FROM ProductDetails where Quantity >= 1"
                        UpdateCommand="UPDATE ProductDetails SET Product_Name = @Product_Name, Quantity = @Quantity, Price = @Price, Product_Specification = @Product_Specification, Depletion_Time = @Depletion_Time WHERE (ProductID = @ProductID)"
                        DeleteCommand="DELETE FROM ProductDetails WHERE (ProductID = @ProductID)">
                        <DeleteParameters>
                            <asp:Parameter Name="ProductID" />
                        </DeleteParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="Product_Name" />
                            <asp:Parameter Name="Quantity" />
                            <asp:Parameter Name="Price" />
                            <asp:Parameter Name="Product_Specification" />
                            <asp:Parameter Name="Depletion_Time" />
                            <asp:Parameter Name="ProductID" />
                        </UpdateParameters>
                    </asp:SqlDataSource>
                    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" CellPadding="4"
                        DataKeyNames="ProductID" DataSourceID="SqlDataSource2" ForeColor="#333333" GridLines="None">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:TemplateField HeaderText="ProductID" InsertVisible="False" SortExpression="ProductID">
                                <EditItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("ProductID") %>' ReadOnly="true"></asp:Label>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("ProductID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Product_Name" SortExpression="Product_Name">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Product_Name") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Product_Name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Quantity" SortExpression="Quantity">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Quantity") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("Quantity") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Price" SortExpression="Price">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Price") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("Price") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Product_Specification" SortExpression="Product_Specification">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Product_Specification") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label5" runat="server" Text='<%# Bind("Product_Specification") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Depletion_Time" SortExpression="Depletion_Time">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("Depletion_Time") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label6" runat="server" Text='<%# Bind("Depletion_Time") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="img" SortExpression="img">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("img") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label7" runat="server" Text='<%# Bind("img") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Image ID="Image1" runat="server" ImageUrl='<%# "~/admin/imges/"+Eval("img")  %>'
                                        Height="50px" Width="50px" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <EditItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update"
                                        Text="Update"></asp:LinkButton>
                                    &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel"
                                        Text="Cancel"></asp:LinkButton>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit"
                                        Text="Edit"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:istsdataConnectionString %>"
                        SelectCommand="SELECT ProductDetails.* FROM ProductDetails where Quantity < 1"
                        UpdateCommand="UPDATE ProductDetails SET Product_Name = @Product_Name, Quantity = @Quantity, Price = @Price, Product_Specification = @Product_Specification, Depletion_Time = @Depletion_Time WHERE (ProductID = @ProductID)"
                        DeleteCommand="DELETE FROM ProductDetails WHERE (ProductID = @ProductID)">
                        <DeleteParameters>
                            <asp:Parameter Name="ProductID" />
                        </DeleteParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="Product_Name" />
                            <asp:Parameter Name="Quantity" />
                            <asp:Parameter Name="Price" />
                            <asp:Parameter Name="Product_Specification" />
                            <asp:Parameter Name="Depletion_Time" />
                            <asp:Parameter Name="ProductID" />
                        </UpdateParameters>
                    </asp:SqlDataSource>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <br />
    <br />
</asp:Content>
