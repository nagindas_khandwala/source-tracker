﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;





public partial class admin_product : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        GridView1.Visible = false;
        GridView2.Visible = false;

    }
   
       protected void Button1_Click1(object sender, EventArgs e)
    {
        
        SqlConnection con = null;
        SqlCommand cmd = null;

        try
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["istsdataConnectionString"].ConnectionString);
            string path = Server.MapPath(@".\imges");
           
            String sql = "insert into ProductDetails(Product_Name,Quantity,Price,Product_Specification,Depletion_Time,img) values(@Product_Name,@Quantity,@Price,@Product_Specification,@Depletion_Time,@img)";
            con.Open();
            cmd = new SqlCommand(sql, con);
            cmd.Parameters.AddWithValue("@Product_Name", prd_name.Text);
            cmd.Parameters.AddWithValue("@Quantity", prd_qty.Text);
            cmd.Parameters.AddWithValue("@Price", prd_price.Text);
            cmd.Parameters.AddWithValue("@Product_Specification", prd_desc.Text);
            cmd.Parameters.AddWithValue("@Depletion_Time", prd_expire.Text);
           if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            if (FileUpload1.HasFile)
            {
                HttpPostedFile file = FileUpload1.PostedFile;
                string fullpath = path + @"\" + file.FileName;
                file.SaveAs(fullpath);
               // cmd.Parameters.AddWithValue("@imgurl",fullpath);
                cmd.Parameters.AddWithValue("@img", Convert.ToString(file.FileName));
            }

            cmd.Connection = con;
         
            cmd.ExecuteNonQuery();
            // result.Text = "Record Inserted";
           
            //  DDLmain.Text = "";
            prd_name.Text = "";
            prd_qty.Text = "";
            prd_price.Text = "";
            prd_desc.Text = "";
            prd_expire.Text = "";

            /*ClientScript.RegisterStartupScript(GetType(), "TestAlert", "alert(\' Record Inserted Successful.\');", true);
            string message = "Record Inserted Successfully.";

            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            sb.Append("<script type = 'text/javascript'>");

            sb.Append("window.onload=function(){");

            sb.Append("alert('");

            sb.Append(message);

            sb.Append("')};");

            sb.Append("</script>");

            ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb.ToString());*/
          


            con.Close();
            Response.Redirect("product.aspx");

        }
        catch (Exception ex)
        {
            // Console.WriteLine(e1.Message);
            throw;
        }

    }

       protected void Button2_Click(object sender, EventArgs e)
       {
           Response.Redirect("product.aspx");
       }


       //protected void Button3_Click(object sender, EventArgs e)
       //{

       //    if (RadioButton1.Checked)
       //    {
       //        GridView1.Visible = true;
               
       //    }
       //    else
       //    {
       //        GridView1.Visible = false;
       //    }

       //    if (RadioButton2.Checked)
       //    {
       //        GridView2.Visible = true;

       //    }
       //    else
       //    {
       //        GridView2.Visible = false;
       //    }
          
       //}

    
       protected void Button4_Click(object sender, EventArgs e)
       {
           GridView1.Visible = true;
           GridView2.Visible = false;

       }
       protected void Button5_Click(object sender, EventArgs e)
       {
           GridView1.Visible = false;
           GridView2.Visible = true;


       }
}

