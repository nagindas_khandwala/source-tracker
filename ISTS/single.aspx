﻿<%@ Page Title="" Language="C#" MasterPageFile="~/client.master" AutoEventWireup="true"
    CodeFile="single.aspx.cs" Inherits="single" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 251px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:DataList ID="DataList1" runat="server" DataKeyField="ProductID" DataSourceID="SqlDataSource1"
        RepeatLayout="Flow">
        <ItemTemplate>
            <table cellpadding="20px" cellspacing="5px">
                <tr>
                    <td align="left">
                        <asp:Image ID="Image1" runat="server" ImageUrl='<%# "~/admin/imges/"+Eval("img")  %>'
                            Height="200px" Width="200px" />
                        <br />
                    </td>
                    <td>
                        <asp:Label ID="ProductIDLabel" runat="server" Text='<%# Eval("ProductID") %>' Style="display: none" />
                        <br />
                        Product Name :
                        <asp:Label CssClass="prd_data" ID="Product_NameLabel" runat="server" Text='<%# Eval("Product_Name") %>' />
                        <br />
                        Available Quantity:
                        <asp:Label ID="PriceLabel" CssClass="prd_data" runat="server" Text='<%# Eval("Quantity") %>' />
                        <br />
                        Price:
                        <asp:Label ID="Label1" CssClass="prd_data" runat="server" Text='<%# Eval("Price") %>' />
                        <br />
                        Product Specification:
                        <asp:Label ID="Label2" CssClass="prd_data" runat="server" Text='<%# Eval("Product_Specification") %>' />
                        <br />
                        Depletion_Time:
                        <asp:Label ID="Depletion_TimeLabel" runat="server" CssClass="prd_data" Text='<%# Eval("Depletion_Time") %>' />
                        <br />
                       
                    </td>
            </table>
        </ItemTemplate>
    </asp:DataList>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:istsdataConnectionString %>"
        SelectCommand="SELECT [ProductID], [Product_Name], [Quantity], [Price], [Product_Specification], [Depletion_Time], [img] FROM [ProductDetails] WHERE ([ProductID] = @ProductID)">
        <SelectParameters>
            <asp:QueryStringParameter Name="ProductID" QueryStringField="ProductID" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <div>
        <table style="width: 100%;">
            <tr>
                <td>
                    Quantity:
                    <asp:TextBox ID="txtQty" CssClass="prd_data" runat="server"></asp:TextBox>
                    <br />
                </td>
            </tr>
            <tr>
                <td align="center" class="style1">
                    <asp:Button ID="order" runat="server" Text="Place Order" OnClick="order_Click1" Style="height: 26px" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
