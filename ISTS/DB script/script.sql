USE [istsdata]
GO
/****** Object:  Table [dbo].[ProductDetails]    Script Date: 02/03/2017 10:42:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductDetails](
	[ProductID] [int] IDENTITY(1,1) NOT NULL,
	[Product_Name] [nvarchar](50) NOT NULL,
	[Quantity] [nvarchar](50) NOT NULL,
	[Price] [nvarchar](50) NOT NULL,
	[Product_Specification] [nvarchar](max) NOT NULL,
	[Depletion_Time] [nvarchar](50) NOT NULL,
	[img] [nvarchar](50) NULL,
 CONSTRAINT [PK_ProductDetails] PRIMARY KEY CLUSTERED 
(
	[ProductID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[ProductDetails] ON
INSERT [dbo].[ProductDetails] ([ProductID], [Product_Name], [Quantity], [Price], [Product_Specification], [Depletion_Time], [img]) VALUES (5, N'uranium', N'0', N'25000', N'very dangerous', N'45', N'Jellyfish.jpg')
INSERT [dbo].[ProductDetails] ([ProductID], [Product_Name], [Quantity], [Price], [Product_Specification], [Depletion_Time], [img]) VALUES (6, N'selium', N'10', N'2000', N'jhg', N'5', N'Koala.jpg')
INSERT [dbo].[ProductDetails] ([ProductID], [Product_Name], [Quantity], [Price], [Product_Specification], [Depletion_Time], [img]) VALUES (7, N'Ura', N'0', N'45000', N'hgbn', N'5', N'Chrysanthemum.jpg')
INSERT [dbo].[ProductDetails] ([ProductID], [Product_Name], [Quantity], [Price], [Product_Specification], [Depletion_Time], [img]) VALUES (8, N'u', N'2', N'6546', N'jhgjh', N'4', N'Hydrangeas.jpg')
SET IDENTITY_INSERT [dbo].[ProductDetails] OFF
/****** Object:  Table [dbo].[OrderStatus]    Script Date: 02/03/2017 10:42:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderStatus](
	[OrderID] [int] NOT NULL,
	[Order_Date] [date] NULL,
	[ProductID] [int] NULL,
	[Product_Name] [nvarchar](50) NULL,
	[CustomerID] [int] NULL,
	[User_Name] [nvarchar](50) NULL,
	[EmailID] [nvarchar](50) NULL,
	[status] [nvarchar](max) NULL,
 CONSTRAINT [PK_OrderStatus] PRIMARY KEY CLUSTERED 
(
	[OrderID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderList]    Script Date: 02/03/2017 10:42:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrderList](
	[OrderID] [int] IDENTITY(1,1) NOT NULL,
	[Order_Date] [date] NULL,
	[Order_Quantity] [nvarchar](50) NULL,
	[ProductID] [int] NULL,
	[Product_Name] [nvarchar](50) NULL,
	[CustomerID] [int] NULL,
	[User_Name] [varchar](50) NULL,
	[EmailID] [nvarchar](50) NULL,
	[Address] [nvarchar](max) NULL,
	[Depletion_Time] [nvarchar](50) NULL,
	[Status] [nvarchar](50) NULL,
 CONSTRAINT [PK_OrderList] PRIMARY KEY CLUSTERED 
(
	[OrderID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[OrderList] ON
INSERT [dbo].[OrderList] ([OrderID], [Order_Date], [Order_Quantity], [ProductID], [Product_Name], [CustomerID], [User_Name], [EmailID], [Address], [Depletion_Time], [Status]) VALUES (21, CAST(0x4A3C0B00 AS Date), N'12', 7, N'Ura', 4, N'div', N'dkc@gmail.com', N'zdfsg', N'5', N'order placed')
INSERT [dbo].[OrderList] ([OrderID], [Order_Date], [Order_Quantity], [ProductID], [Product_Name], [CustomerID], [User_Name], [EmailID], [Address], [Depletion_Time], [Status]) VALUES (22, CAST(0x4A3C0B00 AS Date), N'56', 6, N'selium', 4, N'div', N'dkc@gmail.com', N'zdfsg', N'5', N'order placed')
INSERT [dbo].[OrderList] ([OrderID], [Order_Date], [Order_Quantity], [ProductID], [Product_Name], [CustomerID], [User_Name], [EmailID], [Address], [Depletion_Time], [Status]) VALUES (23, CAST(0x4A3C0B00 AS Date), N'3', 5, N'uranium', 6, N'Divyesh', N'divyeshchauhan1812@gmail.com', N'malad east', N'45', N'order placed')
INSERT [dbo].[OrderList] ([OrderID], [Order_Date], [Order_Quantity], [ProductID], [Product_Name], [CustomerID], [User_Name], [EmailID], [Address], [Depletion_Time], [Status]) VALUES (24, CAST(0x4A3C0B00 AS Date), N'5', 5, N'uranium', 6, N'Divyesh', N'divyeshchauhan1812@gmail.com', N'malad east', N'45', N'order placed')
INSERT [dbo].[OrderList] ([OrderID], [Order_Date], [Order_Quantity], [ProductID], [Product_Name], [CustomerID], [User_Name], [EmailID], [Address], [Depletion_Time], [Status]) VALUES (25, CAST(0x4A3C0B00 AS Date), N'1', 8, N'u', 6, N'Divyesh', N'divyeshchauhan1812@gmail.com', N'malad east', N'4', N'order placed')
INSERT [dbo].[OrderList] ([OrderID], [Order_Date], [Order_Quantity], [ProductID], [Product_Name], [CustomerID], [User_Name], [EmailID], [Address], [Depletion_Time], [Status]) VALUES (26, CAST(0x4A3C0B00 AS Date), N'4', 8, N'u', 6, N'Divyesh', N'divyeshchauhan1812@gmail.com', N'malad east', N'4', N'order placed')
INSERT [dbo].[OrderList] ([OrderID], [Order_Date], [Order_Quantity], [ProductID], [Product_Name], [CustomerID], [User_Name], [EmailID], [Address], [Depletion_Time], [Status]) VALUES (27, CAST(0x4A3C0B00 AS Date), N'5', 6, N'selium', 6, N'Divyesh', N'divyeshchauhan1812@gmail.com', N'malad east', N'5', N'order placed')
INSERT [dbo].[OrderList] ([OrderID], [Order_Date], [Order_Quantity], [ProductID], [Product_Name], [CustomerID], [User_Name], [EmailID], [Address], [Depletion_Time], [Status]) VALUES (28, CAST(0x4A3C0B00 AS Date), N'3', 5, N'uranium', 6, N'Divyesh', N'divyeshchauhan1812@gmail.com', N'malad east', N'45', N'order placed')
INSERT [dbo].[OrderList] ([OrderID], [Order_Date], [Order_Quantity], [ProductID], [Product_Name], [CustomerID], [User_Name], [EmailID], [Address], [Depletion_Time], [Status]) VALUES (29, CAST(0x4A3C0B00 AS Date), N'4', 6, N'selium', 6, N'Divyesh', N'divyeshchauhan1812@gmail.com', N'malad east', N'5', N'order placed')
INSERT [dbo].[OrderList] ([OrderID], [Order_Date], [Order_Quantity], [ProductID], [Product_Name], [CustomerID], [User_Name], [EmailID], [Address], [Depletion_Time], [Status]) VALUES (30, CAST(0x4A3C0B00 AS Date), N'5', 6, N'selium', 7, N'Harsh', N'kamdar400@gamil.com', N'virar', N'5', N'order placed')
INSERT [dbo].[OrderList] ([OrderID], [Order_Date], [Order_Quantity], [ProductID], [Product_Name], [CustomerID], [User_Name], [EmailID], [Address], [Depletion_Time], [Status]) VALUES (31, CAST(0x4A3C0B00 AS Date), N'6', 6, N'selium', 6, N'Divyesh', N'divyeshchauhan1812@gmail.com', N'malad east', N'5', N'order placed')
INSERT [dbo].[OrderList] ([OrderID], [Order_Date], [Order_Quantity], [ProductID], [Product_Name], [CustomerID], [User_Name], [EmailID], [Address], [Depletion_Time], [Status]) VALUES (32, CAST(0x693C0B00 AS Date), N'2', 5, N'uranium', 6, N'Divyesh', N'divyeshchauhan1812@gmail.com', N'malad east', N'45', N'order placed')
SET IDENTITY_INSERT [dbo].[OrderList] OFF
/****** Object:  Table [dbo].[CustomerDetails]    Script Date: 02/03/2017 10:42:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CustomerDetails](
	[CustomerID] [int] IDENTITY(1,1) NOT NULL,
	[User_Name] [varchar](50) NOT NULL,
	[EmailID] [nvarchar](50) NOT NULL,
	[Address] [nvarchar](max) NOT NULL,
	[DOB] [date] NOT NULL,
	[Password] [nvarchar](10) NOT NULL,
	[Country] [nvarchar](50) NOT NULL,
	[State] [nvarchar](50) NOT NULL,
	[City] [nvarchar](50) NULL,
	[Pin_code] [numeric](10, 0) NOT NULL,
 CONSTRAINT [PK_CustomerDetails] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[CustomerDetails] ON
INSERT [dbo].[CustomerDetails] ([CustomerID], [User_Name], [EmailID], [Address], [DOB], [Password], [Country], [State], [City], [Pin_code]) VALUES (3, N'div', N'dkc@gmail.com', N'jhhdgf', CAST(0x59250B00 AS Date), N'123', N'india', N'maharashtra', N'mum', CAST(4009 AS Numeric(10, 0)))
INSERT [dbo].[CustomerDetails] ([CustomerID], [User_Name], [EmailID], [Address], [DOB], [Password], [Country], [State], [City], [Pin_code]) VALUES (4, N'div', N'dkc@gmail.com', N'zdfsg', CAST(0x56250B00 AS Date), N'123', N'India', N'Gujrat', N'mumbai', CAST(40097 AS Numeric(10, 0)))
INSERT [dbo].[CustomerDetails] ([CustomerID], [User_Name], [EmailID], [Address], [DOB], [Password], [Country], [State], [City], [Pin_code]) VALUES (5, N'harsh', N'hk@gmail.cokm', N'kjsdhf', CAST(0x0E210B00 AS Date), N'124', N'India', N'Maharashtra', N'Kaiga', CAST(40097 AS Numeric(10, 0)))
INSERT [dbo].[CustomerDetails] ([CustomerID], [User_Name], [EmailID], [Address], [DOB], [Password], [Country], [State], [City], [Pin_code]) VALUES (6, N'Divyesh', N'divyeshchauhan1812@gmail.com', N'malad east', CAST(0xB21F0B00 AS Date), N'1234', N'India', N'Maharashtra', N'Kaiga', CAST(40097 AS Numeric(10, 0)))
INSERT [dbo].[CustomerDetails] ([CustomerID], [User_Name], [EmailID], [Address], [DOB], [Password], [Country], [State], [City], [Pin_code]) VALUES (7, N'Harsh', N'kamdar400@gamil.com', N'virar', CAST(0xA51F0B00 AS Date), N'123', N'Isreal', N'Karnataka', N'Kaiga', CAST(40096 AS Numeric(10, 0)))
SET IDENTITY_INSERT [dbo].[CustomerDetails] OFF
/****** Object:  Table [dbo].[AdminDetails]    Script Date: 02/03/2017 10:42:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AdminDetails](
	[AdminID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[EmailID] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](10) NOT NULL,
	[Questions] [nvarchar](max) NULL,
	[Answers] [nvarchar](max) NULL,
 CONSTRAINT [PK_AdminDetails] PRIMARY KEY CLUSTERED 
(
	[AdminID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[AdminDetails] ON
INSERT [dbo].[AdminDetails] ([AdminID], [UserName], [EmailID], [Password], [Questions], [Answers]) VALUES (1, N'divyesh Chauhan', N'divyesh@gmail.com', N'123', N'Place of Birth ?', N'mumbai')
INSERT [dbo].[AdminDetails] ([AdminID], [UserName], [EmailID], [Password], [Questions], [Answers]) VALUES (2, N'harsh', N'harsh@gamil.com', N'123', N'Place of Birth ?', N'virar')
SET IDENTITY_INSERT [dbo].[AdminDetails] OFF
