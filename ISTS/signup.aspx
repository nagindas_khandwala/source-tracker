﻿<%@ Page Title="" Language="C#" MasterPageFile="~/client.master" AutoEventWireup="true" CodeFile="signup.aspx.cs" Inherits="signup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
 <link href="style.css" rel="Stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            width: 104px;
        }
        .style2
        {
            width: 27px;
        }
       
        .style3
        {
            width: 415px;
        }
       
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <h1 class="contenth1" align="center"><b>Registration</b></h1>
    <table border="5" cellpadding="5" cellspacing="5" 
        style="width:100%; font-size: small;" rules="none">
        <tr>
            <td class="style2">&nbsp;</td>
            <td class="style1"><b>
                UserName</b></td>
            <td class="style3" align="justify">
                <asp:TextBox ID="username" runat="server" Width="300px" CssClass="textbox" 
                    Height="22px"></asp:TextBox>
            </td>
            <td class="style22">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style2">&nbsp;</td>
            <td class="style1"><b>
                Email_ID</b></td>
            <td class="style3">
                <asp:TextBox ID="email" runat="server" Width="300px" Height="22px"  PlaceHolder="ex:example@gmail.com" 
                    CssClass="textbox"></asp:TextBox>
            </td>
            <td class="style24">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style2">&nbsp;</td>
            <td class="style1"><b> Address</b></td>
            <td class="style3" >
                <asp:TextBox ID="address" runat="server" Columns="40" Rows="5" Style="background: white;
                    border: 1px solid #78BBE6; border-radius: 5px; box-shadow: 0 0 5px 3px #78BBE6;
                    color: #666; outline: none;" TextMode="MultiLine" Height="63px" Width="296px"></asp:TextBox>
            </td>
            <td class="style22">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style2">&nbsp;</td>
            <td class="style1"><b>DOB</b></td>
            <td class="style3" >
                <asp:TextBox ID="dob" runat="server" Width="300px" CssClass="textbox" PlaceHolder="DD/MM/YYYY" 
                    Height="22px"></asp:TextBox>
            </td>
            <td class="style26">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style2">&nbsp;</td>
            <td class="style1">Password</td>
            <td class="style3" >
                
                <asp:TextBox ID="password" runat="server" Width="300px" CssClass="textbox" 
                    Height="22px" TextMode="Password"></asp:TextBox>
                
            </td>
            <td class="style28">
                
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style2">&nbsp;</td>
            <td class="style1">Re-enter Pswd</td>
            <td class="style3" >
                <asp:TextBox ID="confpassword" runat="server" Width="300px" CssClass="textbox" 
                    Height="22px"></asp:TextBox>
            </td>
            <td class="style24">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td class="style1">
                Country</td>
            <td class="style3" >
                <asp:DropDownList ID="country" runat="server" CssClass="textbox" 
                    Height="25px" Width="300px" 
                   >
                    <asp:ListItem>select country</asp:ListItem>
                    <asp:ListItem>GermanyGermany</asp:ListItem>
                    <asp:ListItem>India</asp:ListItem>
                    <asp:ListItem>Isreal</asp:ListItem>
                    <asp:ListItem>Nepal</asp:ListItem>
                   
                    <asp:ListItem>North Korea</asp:ListItem>
                    <asp:ListItem>SouthAfrica</asp:ListItem>
                   
                </asp:DropDownList>
                        
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td class="style1">
                State</td>
            <td class="style3" >
                <asp:DropDownList ID="state" runat="server" CssClass="textbox" Height="25px" 
                    Width="300px">
                    <asp:ListItem>select state</asp:ListItem>
                   
                    <asp:ListItem>Gujrat</asp:ListItem>
                    <asp:ListItem>Karnataka</asp:ListItem>
                    <asp:ListItem>Maharashtra</asp:ListItem>
                    <asp:ListItem>Rajastan</asp:ListItem>
                    <asp:ListItem>Tamil Nadu</asp:ListItem>
                   
                </asp:DropDownList>
                        
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td class="style1">
                City</td>
            <td class="style3" >
                <asp:DropDownList ID="city" runat="server" CssClass="textbox" Height="25px" 
                    Width="300px">
                    <asp:ListItem>select city</asp:ListItem>
                    <asp:ListItem>Tarapura</asp:ListItem>
                    <asp:ListItem>Kaiga</asp:ListItem>
                    <asp:ListItem>KudanKulam</asp:ListItem>
                    <asp:ListItem>Kakrapura</asp:ListItem>
                   
                </asp:DropDownList>
                        
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td class="style1">
                Pin Code</td>
            <td class="style3" >
                <asp:TextBox ID="pin" runat="server" Width="300px" Height="22px"  
                    CssClass="textbox"></asp:TextBox>
                        
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td class="style1">
                &nbsp;</td>
            <td class="style3" >
                &nbsp;</td>
            <td class="style2">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td class="style1">
                &nbsp;</td>
            <td class="style3" >
                <asp:Button ID="Button1" runat="server" Text="Submit" CssClass="btn" 
                    Height="42px" Width="99px" onclick="Button1_Click"
                   />
                &nbsp;
                    
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td class="style1">
                &nbsp;</td>
            <td class="style3" >
                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/login.aspx">&lt;- Back to Login Page</asp:HyperLink>
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr>
        </table>
</asp:Content>

