﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

public partial class signup : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        SqlConnection con = null;
        SqlCommand cmd = null;

        try
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["istsdataConnectionString"].ConnectionString);

            String sql = "insert into CustomerDetails(User_Name,EmailID,Address,DOB,Password,Country,State,City,Pin_code) values(@User_Name,@EmailID,@Address,@DOB,@Password,@Country,@State,@City,@Pin_code)";
            con.Open();
            cmd = new SqlCommand(sql, con);
           
            cmd.Parameters.AddWithValue("@User_Name",username.Text);
            cmd.Parameters.AddWithValue("@EmailID", email.Text);
            cmd.Parameters.AddWithValue("@Address",address.Text);
            cmd.Parameters.AddWithValue("@DOB",dob.Text);
            cmd.Parameters.AddWithValue("@Password",password.Text);
            cmd.Parameters.AddWithValue("@Country", country.SelectedValue);
            cmd.Parameters.AddWithValue("@State", state.SelectedValue);
            cmd.Parameters.AddWithValue("@City", city.SelectedValue);
            cmd.Parameters.AddWithValue("@Pin_code", pin.Text);
           

            cmd.Connection = con;
            
            username.Text = "";
            email.Text = "";
            address.Text = "";
            dob.Text = "";
            password.Text = "";
            country.SelectedIndex = 0;
            state.SelectedIndex = 0;
            city.SelectedIndex = 0;
            pin.Text="";

            cmd.ExecuteNonQuery();
            /*string message = "Record Inserted Successfully.";

            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            sb.Append("<script type = 'text/javascript'>");

            sb.Append("window.onload=function(){");

            sb.Append("alert('");

            sb.Append(message);

            sb.Append("')};");

            sb.Append("</script>");

            ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb.ToString());*/
            con.Close();
            Response.Redirect("login.aspx");
        }
        catch (Exception)
        {
           
            
        }
    }
    
}
